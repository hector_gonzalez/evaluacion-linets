from django.urls import path, include
from .views import desplegar_prod, ProductoViewSet
from rest_framework import routers

router = routers.DefaultRouter()
router.register('datos', ProductoViewSet)

urlpatterns = [
    path('', desplegar_prod, name='index'),
    path('api/', include(router.urls))
]
from django.shortcuts import render
from .models import MasterProductsConfigurable
from rest_framework import viewsets
from .serializers import ProductoSerializer

# Create your views here.

def desplegar_prod(request):
    return render(request, 'sentencia/sentencia.html')
    

class ProductoViewSet(viewsets.ModelViewSet):
    queryset = MasterProductsConfigurable.objects.all()
    serializer_class = ProductoSerializer
from rest_framework import serializers
from .models import MasterProductsConfigurable

class ProductoSerializer(serializers.ModelSerializer):

    class Meta:
        model = MasterProductsConfigurable
        fields = [
        'model','group_by_model',
        'sku','yr_es','yr_fr','name_single_product',
        'name','summary','qty','ean1','color_group_hex','color_child',
        'status','weight','brand',
        'price','special_price',
        'short_description','long_description',
        'cross_selling','cross_selling_cart',
        'size_chart','image1','image2','image3','image4','image5','image6','image7',
        'image8','image9','application_advice','attribute_format_container','vegetal_active',
        'attribute_container','attribute_coverage','inci','variant_icon','attribute_face',
        'attribute_esencia','attribute_line','attribute_hair_type',
        'attribute_type_perfume','attribute_effect','attribute_texture',
        'attribute_type_skin','attribute_zone','attribute_color',
        'attribute_ip_solar','attribute_action','attribute_class','attribute_type_product',
        'attribute_type_care','attribute_need','category_1_1','category_1_2','category_1_3',
        'category_2_1','category_2_2','category_2_3','category_3_1','category_3_2','category_3_3',
        'category_4_1','category_4_2','category_4_3','category_5_1','category_5_2','category_5_3',
        'category_6_1','category_6_2','category_6_3','category_7_1','category_7_2','category_7_3',
        'category_8_1','category_8_2','category_8_3','category_9_1','category_9_2','category_9_3',
        'category_10_1','category_10_2','category_10_3','category_11_1','category_11_2','category_11_3',
        'category_12_1','category_12_2','category_12_3','category_13_1','category_13_2','category_13_3',
        'category_14_1','category_14_2','category_14_3','attribute_beauty']

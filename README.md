# Evaluación Linets

### Introducción
- Esta es la documentación paso a paso, para solucionar el problema propuesto dentro de la evaluación, por medio de los requerimientos plasmados en la misma.


- El Sistema debe ser montado en Docker.
- El proyecto debe estar versionado en Git.
- Se debe utilizar django framework para generar el comando.

### Etapa 1°: Preparación
- Debido a las condiciones previas, se requiere disponer de distintos softwares. Por lo cual el primer paso es instalar los programas necesarios (Docker, un software de programación, etc)

### Etapa 2°: Desarrollo
- Se debe generar un nuevo proyecto, creando una carpeta en una ruta a elección. Dentro del software de programación escogido, se debe desplegar dentro de la carpeta previamante creada, todos los archivos necesarios para el funcionamiento del sistema (comandos que provee python, como: startproject, startapp, etc).
- Una vez ya tenemos la base de nuestro proyecto, necesitaremos implementar .git. Por lo cual se crea la rama master dentro de la carpeta deseada.
- Una vex terminado ese proceso, podemos empezar a trabajar dentro de los archivos de python y hacer uso de las herramientas provistas por el lenguaje para solucionar nuestra problemática.
- Se debe crear siempre la carpeta de "templates", que es donde irá nuestro archivo .html, ya que es en esa carpeta donde python busca nuestras plantillas creadas.
- Se debe crear el modelo de datos propuesto en el caso. Por medio de "inspectdb" y cambiando la dirección de la bd en settings.py, eso fue posible.
- Se deben crear las vistas (en este caso "desplegar_prod" y "ProductoViewSet"), url's (en este caso "/api/" e "index" necesarias.
- Se debe crear la sentencia que genere el fichero .csv y luego implementar bootstrap según lo solicitado.
